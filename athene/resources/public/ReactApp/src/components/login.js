import React from 'react'
import '../stylesheets/style.scss'

import logo from '../img/logo.png'

require('babel-polyfill');


export const Login = React.createClass ({

 requestBuildQueryString: function (params) {
    var queryString = [];
    for(var property in params)
      if (params.hasOwnProperty(property)) {
        queryString.push(encodeURIComponent(property) + '=' + encodeURIComponent(params[property]));
      }
    return queryString.join('&');
 },

 displaingsuccessMessage: function () {
    // displing the finishing message
    var form = document.getElementsByClassName('logining__form')[0],
        wraper = document.getElementsByClassName('wraper')[0],
        formStyle = window.getComputedStyle(form, null),
        wrapStyle = window.getComputedStyle(wraper, null);

    form.style.left = '-9999px';
  },

loginAction: function (e) {

    // stop the submiting
    e.preventDefault();

    // Fetch form values.
    var formData = {
     login:     this.refs.login.value,
     password:  this.refs.password.value
    };

    console.log(formData);

   // setting white lighbox
   var opacityId = document.getElementsByClassName("wraper")[0];
       opacityId.style.opacity = "0.7";

   // blocking submit button
   var formSubmit = document.getElementsByClassName("form__button")[0];
       formSubmit.setAttribute("disabled", "disabled");

   var loader = document.getElementsByClassName('loader')[0];
       loader.style.display = "block";

    (async() => {
       try {
         const response = await fetch('/login', {
                               method: 'post',
                               headers:{'content-type': 'application/json'},
                               body: JSON.stringify({
                                 login:     this.refs.login.value,
                                 password:  this.refs.password.value
                               })
                              });

         // resieving the server data
         const data = JSON.parse(await response.text());

         console.log(data);

         var boxes = document.getElementsByClassName('form__status');

         // shaking the inputs if they are uncorrect
         var input = document.getElementsByClassName('logining__form-element');
         var shakingElements = [];

         function shake (element, magnitude = 5) {
           var counter = 1;
           var numberOfShakes = 15;

           //Capture the element's position
           var startX = 0,
               startY = 0;

           // Divide the magnitude into 10 units so that you can reduce the amount of shake by 10 percent each frame
           var magnitudeUnit = magnitude / numberOfShakes;

           //The `randomInt` helper function
           var randomInt = (min, max) => {
             return Math.floor(Math.random() * (max - min + 1)) + min;
           };

           //Add the element to the `shakingElements` array
           if(shakingElements.indexOf(element) === -1) {
             shakingElements.push(element);
             upAndDownShake();
           }


           function upAndDownShake() {
             //Shake the element while the `counter` is less than the `numberOfShakes
             if (counter < numberOfShakes) {

               //Reset the element's position at the start of each shake
               element.style.transform = 'translate(' + startX + 'px, ' + startY + 'px)';

               //Reduce the magnitude
               magnitude -= magnitudeUnit;

               //Randomly change the element's position
               var randomX = randomInt(-magnitude, magnitude);
               var randomY = randomInt(-magnitude, magnitude);

               element.style.transform = 'translate(' + randomX + 'px, ' + randomY + 'px)';

               //Add 1 to the counter
               counter += 1;

               requestAnimationFrame(upAndDownShake);
             }

             //When the shaking is finished, restore the element to its original
             //position and remove it from the `shakingElements` array
             if (counter >= numberOfShakes) {
               element.style.transform = 'translate(' + startX + ', ' + startY + ')';
               shakingElements.splice(shakingElements.indexOf(element), 1);
             }
           }

         };


         // changing status if there are some errors...
         switch (data) {
           case "Error":
             opacityId.style.opacity = "1.0";
             loader.style.display = "none";
             boxes[0].className = "form__status form__status-fail";
             boxes[1].className = "form__status form__status-fail";

             // shaking form elements
             for (var i = 0; i <= 1; i++) {
               shake(input[i]);
             }
             break;

          case "Success":
             opacityId.style.opacity = "1.0";
             loader.style.display = "none";
             boxes[0].className = "form__status form__status-succes";
             boxes[1].className = "form__status form__status-succes";

             // displing the finishing message
             var form = document.getElementsByClassName('logining__form')[0],
                 wraper = document.getElementsByClassName('wraper')[0],
                 formStyle = window.getComputedStyle(form, null),
                 wrapStyle = window.getComputedStyle(wraper, null);

             window.location.replace("/");
             break;
          }

          // unblocking submit button
          var formSubmit = document.getElementsByClassName("form__button")[0];
              formSubmit.removeAttribute("disabled");
       } catch (e) {
         console.log(e);
       }
     })();

  },

  // centering the main form.
  formStyling: function () {
    window.onload = function () {
      var form      = document.getElementsByClassName('logining__form')[0],
          wraper    = document.getElementsByClassName('wraper')[0],
          formStyle = window.getComputedStyle(form, null),
          wrapStyle = window.getComputedStyle(wraper, null);

          form.style.top = (parseInt(wrapStyle.height) - parseInt(formStyle.height)) / 2 + 'px';
          form.style.left = (parseInt(wrapStyle.width) - parseInt(formStyle.width)) / 2 + 'px';
    }
  },

  render() {
    return (
      <div className="wraper" ref="wraper">
        <div className="logining__form" ref="logining__form">
          <form  method="POST" action="/install" onSubmit={this.loginAction}>
            <div className="logo__wrap">
              <img src={logo} alt="" className="logo" />
            </div>

            <div className="submiting_status-wrap">
              <h6 className="submiting_status__title">Logining to the system</h6>
            </div>

            <label htmlFor="dbname" className="logining-form__label">
              <span className="form__status">&nbsp;</span>
              <input type="text"     name="email"     ref="login"     id="login"     className="logining__form-element"  placeholder="Login"  required/>
            </label>

    				<label htmlFor="password" className="logining-form__label">
              <span className="form__status">&nbsp;</span>
              <input type="password" name="pwd" ref="password" id="password" className="logining__form-element"  placeholder="Password"  />
            </label>

            <label className="form__loader"><span className="loader">&nbsp;</span><input className="form__button" type="submit" value="Submit"/></label>
          </form>
        </div>

        {this.formStyling()}
    	</div>
    )
  }
})
