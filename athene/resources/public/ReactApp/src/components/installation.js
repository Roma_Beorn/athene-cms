import React from 'react'
import '../stylesheets/style.scss'

import logo from '../img/logo.png'

require('babel-polyfill');


export const InstallationForm = React.createClass ({

 requestBuildQueryString: function (params) {
    var queryString = [];
    for(var property in params)
      if (params.hasOwnProperty(property)) {
        queryString.push(encodeURIComponent(property) + '=' + encodeURIComponent(params[property]));
      }
    return queryString.join('&');
 },

 displaingsuccessMessage: function () {
    // displing the finishing message
    var form = document.getElementsByClassName('installation__form')[0],
        wraper = document.getElementsByClassName('wraper')[0],
        formMess = document.getElementsByClassName('success__form')[0],
        formStyle = window.getComputedStyle(form, null),
        wrapStyle = window.getComputedStyle(wraper, null),
        formMessStyle = window.getComputedStyle(formMess, null);

    form.style.left = '-9999px';
    formMess.style.left = (parseInt(wrapStyle.width) - parseInt(formMessStyle.width)) / 2 + 'px';
  },

  installAction: function (e) {

    // stop the submiting
    e.preventDefault();

    // Fetch form values.
    var formData = {
     dbname:     this.refs.dbname.value,
     dbaddress:  this.refs.dbaddress.value,
     dbuser:     this.refs.dbuser.value,
     dbpassword: this.refs.dbpassword.value
    };

   // setting white lighbox
   var opacityId = document.getElementsByClassName("wraper")[0];
       opacityId.style.opacity = "0.7";

   // blocking submit button
   var formSubmit = document.getElementsByClassName("form__button")[0];
       formSubmit.setAttribute("disabled", "disabled");

   var loader = document.getElementsByClassName('loader')[0];
       loader.style.display = "block";

   var error_field = document.getElementsByClassName('error_field')[0];
       error_field.innerHTML = "Loading...";

    (async() => {
       try {
         const response = await fetch('/install', {
                               method: 'post',
                               headers:{'content-type': 'application/json'},
                               body: JSON.stringify({
                                 dbname:     this.refs.dbname.value,
                                 dbaddress:  this.refs.dbaddress.value,
                                 dbuser:     this.refs.dbuser.value,
                                 dbpassword: this.refs.dbpassword.value
                               })
                              });

         // resieving the server data
         const data = JSON.parse(await response.text());

         var boxes = document.getElementsByClassName('form__status'),
             error_field = document.getElementsByClassName('error_field')[0];


         // status function
         function statusRender (count) {
           var circle = document.getElementsByClassName('list_element__circle ');

           // reset status
           for (var i = 0; i <= 3; i++) { circle[i].className = "list_element__circle" }

           // rendering status
           if (count <= 4) {
             for (var i = 0; i < count - 1; i++) {
               circle[i].className = "list_element__circle circle_succes"
             }
             circle[count - 1].className = "list_element__circle circle_fail";
            } else {
              for (var i = 0; i <= 3; i++) {
                circle[i].className = "list_element__circle circle_succes"
              }
            }
         }


         // shaking the inputs if they are uncorrect
         var input = document.getElementsByClassName('installation__form-element');
         var shakingElements = [];

         function shake (element, magnitude = 5) {
           var counter = 1;
           var numberOfShakes = 15;

           //Capture the element's position
           var startX = 0,
               startY = 0;

           // Divide the magnitude into 10 units so that you can reduce the amount of shake by 10 percent each frame
           var magnitudeUnit = magnitude / numberOfShakes;

           //The `randomInt` helper function
           var randomInt = (min, max) => {
             return Math.floor(Math.random() * (max - min + 1)) + min;
           };

           //Add the element to the `shakingElements` array
           if(shakingElements.indexOf(element) === -1) {
             shakingElements.push(element);
             upAndDownShake();
           }


           function upAndDownShake() {
             //Shake the element while the `counter` is less than the `numberOfShakes
             if (counter < numberOfShakes) {

               //Reset the element's position at the start of each shake
               element.style.transform = 'translate(' + startX + 'px, ' + startY + 'px)';

               //Reduce the magnitude
               magnitude -= magnitudeUnit;

               //Randomly change the element's position
               var randomX = randomInt(-magnitude, magnitude);
               var randomY = randomInt(-magnitude, magnitude);

               element.style.transform = 'translate(' + randomX + 'px, ' + randomY + 'px)';

               //Add 1 to the counter
               counter += 1;

               requestAnimationFrame(upAndDownShake);
             }

             //When the shaking is finished, restore the element to its original
             //position and remove it from the `shakingElements` array
             if (counter >= numberOfShakes) {
               element.style.transform = 'translate(' + startX + ', ' + startY + ')';
               shakingElements.splice(shakingElements.indexOf(element), 1);
             }
           }

         };


         // changing status if there are some errors...
         switch (data.message) {
           case "Host Is Not Accessable":
             opacityId.style.opacity = "1.0";
             loader.style.display = "none";
             boxes[0].className = "form__status form__status-fail";
             boxes[1].className = "form__status";
             boxes[2].className = "form__status";
             boxes[3].className = "form__status";

             // status rendering function
             statusRender(1);

             // shaking form elements
             shake(input[0]);

             // writing error
             error_field.innerHTML = "Host Is Not Accessable";
             break;

           case "Access Denied":
             opacityId.style.opacity = "1.0";
             loader.style.display = "none";
             boxes[0].className = "form__status form__status-succes";
             boxes[1].className = "form__status form__status-fail";
             boxes[2].className = "form__status form__status-fail";
             boxes[3].className = "form__status";

             // status rendering function
             statusRender(2);

             // shaking form elements
             for (var i = 1; i <= 2; i++) {
               shake(input[i]);
             }


             // writing error
             error_field.innerHTML = "Access Denied";
             break;

             case "Unknown Database":
             opacityId.style.opacity = "1.0";
             loader.style.display = "none";
             boxes[0].className = "form__status form__status-succes";
             boxes[1].className = "form__status form__status-succes";
             boxes[2].className = "form__status form__status-succes";
             boxes[3].className = "form__status form__status-fail";

             // status rendering function
             statusRender(3)

             // shaking form elements
             shake(input[3]);

             // writing error
             error_field.innerHTML = "Unknown Database";
             break;

             case "Connected":
             opacityId.style.opacity = "1.0";
             loader.style.display = "none";
             boxes[0].className = "form__status form__status-succes";
             boxes[1].className = "form__status form__status-succes";
             boxes[2].className = "form__status form__status-succes";
             boxes[3].className = "form__status form__status-succes";

             // status rendering function
             statusRender(5);

             // writing status and redirecting
             error_field.innerHTML = "Connected";

             // displing the finishing message
             var form = document.getElementsByClassName('installation__form')[0],
                 wraper = document.getElementsByClassName('wraper')[0],
                 formMess = document.getElementsByClassName('success__form')[0],
                 formStyle = window.getComputedStyle(form, null),
                 wrapStyle = window.getComputedStyle(wraper, null),
                 formMessStyle = window.getComputedStyle(formMess, null);

             form.style.left = '-5000px';
             formMess.style.right = (parseInt(wrapStyle.width) - parseInt(formMessStyle.width)) / 2 + 'px';

             // window.location.replace("/");
             break;
          }

          // unblocking submit button
          var formSubmit = document.getElementsByClassName("form__button")[0];
              formSubmit.removeAttribute("disabled");
       } catch (e) {
         console.log("Не получи лося =)");
       }
     })();

  },

  // centering the main form.
  formStyling: function () {
    window.onload = function () {
      var form      = document.getElementsByClassName('installation__form')[0],
          wraper    = document.getElementsByClassName('wraper')[0],
          formMess = document.getElementsByClassName('success__form')[0],
          formStyle = window.getComputedStyle(form, null),
          wrapStyle = window.getComputedStyle(wraper, null),
          formMessStyle = window.getComputedStyle(formMess, null);

          form.style.top = (parseInt(wrapStyle.height) - parseInt(formStyle.height)) / 2 + 'px';
          form.style.left = (parseInt(wrapStyle.width) - parseInt(formStyle.width)) / 2 + 'px';

          formMess.style.top = (parseInt(wrapStyle.height) - parseInt(formMessStyle.height)) / 2 + 'px';
    }
  },

  render() {
    return (
      <div className="wraper" ref="wraper">
        <div className="installation__form" ref="installation__form">
      		<div className="form_left">
            <form  method="POST" action="/install" onSubmit={this.installAction}>
      				<label htmlFor="dbaddress" className="installation-form__label">
                <span className="form__status">&nbsp;</span>
                <input type="text"     name="dbaddress"  ref="dbaddress"  id="dbaddress"  className="installation__form-element"  placeholder="Host IP"   required/>
              </label>

      				<label htmlFor="dbuser" className="installation-form__label">
                <span className="form__status">&nbsp;</span>
                <input type="text"     name="dbuser"     ref="dbuser"     id="dbuser"     className="installation__form-element"  placeholder="Login"   required/>
              </label>

      				<label htmlFor="dbpassword" className="installation-form__label">
                <span className="form__status">&nbsp;</span>
                <input type="password" name="dbpassword" ref="dbpassword" id="dbpassword" className="installation__form-element"  placeholder="Password"  />
              </label>

              <label htmlFor="dbname" className="installation-form__label">
                <span className="form__status">&nbsp;</span>
                <input type="text"     name="dbname"     ref="dbname"     id="dbname"     className="installation__form-element"  placeholder="DB name"  required/>
              </label>

              <label className="form__loader"><span className="loader">&nbsp;</span><input className="form__button" type="submit" value="Install"/></label>
            </form>
      		</div>
      		<div className="form_right">
            <div className="logo__wrap">
              <img src={logo} alt="" className="logo" />
            </div>

            <div className="submiting_status-wrap">
              <h6 className="submiting_status__title">Database Integration Step</h6>
              <ul className="submiting_status__list">
                <li className="status_list__element"><span className="list_element__circle"></span>Connected to Host</li>
                <li className="status_list__element"><span className="list_element__circle"></span>User Validation</li>
                <li className="status_list__element"><span className="list_element__circle"></span>Database Employment</li>
                <li className="status_list__element"><span className="list_element__circle"></span>Creating Config File</li>
              </ul>
            </div>

            <div className="error_field__wrap">
              <span className="error_field">...</span>
            </div>
      		</div>
        </div>

        <div className="success__form">
          <div className="logo__wrap">
            <img src={logo} alt="" className="logo" />
          </div>
          <h4 className="success_form__title">Installation Completed</h4>
          <p className="success_form__message">You are welcome! Installation completed successfully. <br /> <span>Press F5</span> for further employment</p>
        </div>

        {this.formStyling()}
    	</div>
    )
  }
})
