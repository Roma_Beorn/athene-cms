import React from 'react'
import text from './titles.json'
import './stylesheets/installationFrom.scss'


//временная инфа
export const hello = (
  <h1>{text.hello}</h1>
)


// форма установки
export const installationForm = (
  <div className="wraper">
		<form className="installation__form"  method="POST" action="/install">
			<div className="form__top">
				<h3>Регистрация</h3>
				<p>Установка Athene CMS - шаг 1</p>
				<input type="text"     name="dbname"     className="installation__form-element" required  placeholder="Название БД" />
				<input type="text"     name="dbaddress"  className="installation__form-element" required  placeholder="IP хоста"  />
				<input type="text"     name="dbuser"     className="installation__form-element" required  placeholder="Имя пользователя"  />
				<input type="password" name="dbpassword" className="installation__form-element" required  placeholder="Пароль"  />
			</div>
			<div className="form__bottom">
				<input className="form__button" type="submit" value="Продолжить" />
			</div>
		</form>
	</div>
)
