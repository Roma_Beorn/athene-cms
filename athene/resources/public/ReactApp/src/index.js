import React from 'react'
import { render } from 'react-dom'
import { InstallationForm } from './components/installation'
import { Login } from './components/login'
import { Registration } from './components/registration'

render(
	<div>
		<Login />
	</div>,
	document.getElementById('react-container')
)
