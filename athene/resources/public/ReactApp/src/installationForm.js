import React from 'react'
import './stylesheets/installationFrom.scss'

var installationForm2 = React.createClass ({

  render: function (){
      return (

        <div className="wraper">
          <form className="registration__form"  method="POST" action="/install">
            <div className="form__top">
              <h3>Регистрация</h3>
              <p>Заполните следующие данные</p>
              <input type="text"     name="dbuser"      ref="dbuser" className="installation__form-element" required  placeholder="Email"  />
              <input type="password" name="dbpassword"  ref="dbpassword" className="installation__form-element" required  placeholder="Пароль"  />
              <input type="password" name="dbpassword2" ref="dbpassword2" className="installation__form-element" required  placeholder="повторите пароль" />
            </div>
            <div className="form__bottom">
              <input className="form__button" type="submit" value="Войти" />
            </div>
          </form>
        </div>
      )
  }
});
