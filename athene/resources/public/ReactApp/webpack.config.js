var webpack = require("webpack");
var folder = __dirname;

module.exports = {
  entry: folder + "/src/index.js",
  output: {
    path: folder + "/dist/assets/",
    filename: "bundle.js",
    publicPath: "/assets"
  },
  devServer: {
    inline: true,
    contentBase: folder + "\\dist",
    port: 3001
  },
	module: {
        loaders: [
        {
            test: /\.js$/,
            exclude: /(node_modules)/,
            loader: folder + '\\node_modules\\babel-loader',
            query: {
                presets: ['latest', 'stage-0', 'react']
            }
        },
				{
						test: /\.json$/,
						exclude: /(node_modules)/,
						loader: 'json-loader'
				},
				{
					test: /\.css$/,
					loader: 'style-loader!css-loader!autoprefixer-loader'
				},
        {
          test: /\.scss$/,
					loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader'
        },
        {
          test: /\.(jpg|png|svg|gif)$/,
          loader: 'url-loader'
        }
      ]
    }
}
